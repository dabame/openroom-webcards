/** 
* The Quiz Object 
**/
var Quiz = function() {
    this.upSides = new Array();
    this.flipSides = new Array();
    this.curFace = "";
    this.curIndex = 0;
    this.curSide = "up";
    
    var curRow = document.getElementById("row0").nextSibling;

    var i = 0;
    
    while (curRow.nextSibling) {
	this.upSides[i] = curRow.childNodes[0].innerHTML;
	this.flipSides[i++] = curRow.childNodes[1].innerHTML;
	
	curRow = curRow.nextSibling.nextSibling;
    }
    
    this.curFace = this.upSides[0];
};

// updates curCard div to contain elements of the current card
Quiz.prototype.updateFace = function() {
    if (this.curSide == "up") {
	this.curFace = this.upSides[this.curIndex];
    } else {
	this.curFace = this.flipSides[this.curIndex];
    }

    document.getElementById("curCard").getElementsByTagName("p")[0].innerHTML = this.curFace;
}

// Flips the card between up and flip and updates curFace
Quiz.prototype.flip = function() {
    if (this.curSide == "up") {
	this.curSide = "flip";
    } else {
	this.curSide = "up";
    }

    this.updateFace();
};

// moves to the next card
Quiz.prototype.next = function() {
    if (this.curIndex < this.upSides.length - 1) {
	this.curIndex++;
	this.curSide = "up";
	this.updateFace();
    } else {
	document.getElementById("curCard").getElementsByTagName("p")[0].innerHTML = "Stack Complete!";
    }
}

//moves to the previous card
Quiz.prototype.prev = function () {
    if (this.curIndex > 0) {
	this.curIndex--;
	this.curSide = "up";
	this.updateFace();
    }
}

// build the UI for the quiz and add it to the DOM
Quiz.prototype.buildUI = function() {
    var output = document.createElement("div");

    // build the card display div
    var newElement = document.createElement("div");
    newElement.setAttribute("id", "curCard");
    newElement.setAttribute("class", "jumbotron");
    var newPar = document.createElement("p");
    newElement.appendChild(newPar);

    output.appendChild(newElement);

    // build the navigation div
    newElement = document.createElement("div");
    newElement.setAttribute("id", "quizNav");

    // build the previous button
    newButton = document.createElement("button");
    newButton.setAttribute("onclick", "quiz.prev();");
    newButton.setAttribute("class", "btn-default btn-lg qz-btn");
    var newText = document.createTextNode("Prev");
    newButton.appendChild(newText);
    newElement.appendChild(newButton);

    // build the flip button
    newButton = document.createElement("button");
    newButton.setAttribute("onclick", "quiz.flip();");
    newButton.setAttribute("class", "btn-info btn-lg qz-btn");
    newText = document.createTextNode("Flip");
    newButton.appendChild(newText);
    newElement.appendChild(newButton);

    // build the next button
    newButton = document.createElement("button");
    newButton.setAttribute("onclick", "quiz.next();");
    newButton.setAttribute("class", "btn-default btn-lg qz-btn");
    newText = document.createTextNode("Next");
    newButton.appendChild(newText);
    newElement.appendChild(newButton);

    // build the done button
    newButton = document.createElement("button");
    newButton.setAttribute("onclick", "quiz.destroyUI();");
    newButton.setAttribute("class", "btn-link");
    newText = document.createTextNode("Done");
    newButton.appendChild(newText);
    newElement.appendChild(newButton);
    
    output.appendChild(newElement);
    
    return output;
};

// destroys the quiz UI and shows the stack-view
Quiz.prototype.destroyUI = function() {
    $("#stack-view").show();

    var quizDiv = document.getElementById("quiz-display").childNodes[1];
    quizDiv.parentNode.removeChild(quizDiv);
}

function takeQuiz() {
    // start at the first card
    quiz = new Quiz();

    document.getElementById("quiz-display").appendChild(quiz.buildUI());

    var startText = document.createTextNode(quiz.upSides[0]);
    document.getElementById("curCard").getElementsByTagName("p")[0].appendChild(startText);

    $("#stack-view").hide();
}
