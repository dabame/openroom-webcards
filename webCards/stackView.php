<!DOCTYPE = "html"/>
<html>
    <head>
	
	<meta charset = "UTF-8"/>
	<meta http-equiv = "X-UA-Compatible" content = "IE=edge"/>
	<meta name = "viewport" content = "width=device-width, initial-scale=1"/>

	<!-- Bootstrap CDN -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href ="css/webCards.css"/>  
	
	<title>Openroom | Webcards</title>
	
    </head>
    <?php
    require_once ("lib/Class.WebCardStack.inc.php");
    $stack = new WebcardStack();


    $stack->readFromDb($_GET["id"]);
    ?>
    <body>
	<div class = "container">
	    <div class = "row">
		<div class = "col-md-3">
		    <nav>
			<a href = "index.php">WebCards Home</a>
		    </nav>
		</div>
		<div class = "col-md-9">
		    <h1>Openroom <small>Webcards</small></h1>
		    <div id = "quiz-display">
		    </div>
		    <div id = "stack-view">
			<?php echo $stack->tableDisplay(); ?>
		    </div>
		</div>
	    </div>
	</div>
	
	
	<!-- scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="js/webCards.js"></script>
    </body>
</html>
