<?php

require_once "../lib/Class.WebCard.inc.php";

$webCard = new WebCard();

$webCard->id = $_GET["id"];
$webCard->stackID = $_GET["stackID"];

$webCard->removeFromDb();

header("Location: /webCards/stackView.php?id=" . $webCard->stackID);

?>