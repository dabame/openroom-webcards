<?php

require_once "../lib/Class.WebCardStack.inc.php";

$stack = new WebCardStack();

$stack->stackName = filter_var($_GET["stackName"], FILTER_SANITIZE_STRING);
$stack->stackOwner = $_GET["stackOwner"];
$stack->description = filter_var($_GET["description"], FILTER_SANITIZE_STRING);

$stack->writeNewToDb();

header("Location: /webCards/index.php");

?>
