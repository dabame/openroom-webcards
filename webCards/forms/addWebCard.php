<?php

require_once "../lib/Class.WebCard.inc.php";

$webCard = new WebCard();

$webCard->upSide = filter_var($_GET["upSide"], FILTER_SANITIZE_STRING);
$webCard->flipSide = filter_var($_GET["flipSide"], FILTER_SANITIZE_STRING);
$webCard->cardOwner = $_GET["cardOwner"];
$webCard->stackID = $_GET["stackID"];

$webCard->writeNewToDb();

header("Location: /webCards/stackView.php?id=" . $webCard->stackID);

?>