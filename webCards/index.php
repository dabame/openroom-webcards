<!DOCTYPE = "html"/>
<html>
    <head>
	<meta charset = "UTF-8"/>
	<meta http-equiv = "X-UA-Compatible" content = "IE=edge"/>
	<meta name = "viewport" content = "width=device-width, initial-scale=1"/>
	
	<!-- stylesheets -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/webCards.css"/> 
          
	<title>Openroom | Webcards</title>
    </head>
    <?php
    require_once "lib/Display.WebCardsMenu.php";
    ?>
    <body>

	<div class = "container">
	    <div class = "row">
		<div class = "col-md-3">
		</div>
		<div class = "col-md-9">
		    <h1>Openroom <small>Webcards</small></h1>
		    <div>
			<?php echo displayStacks(); echo newStackForm(); ?>          
		    </div>
		</div>
	    </div>
	</div>
	
	
	<!-- scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>  
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>
