<?php

// represents one experience
class Experience {
    var $organization;
    var $position;
    var $duration;
    var $description;
}

/**
 * Represents a users profile
 **/
class Profile {
    // Name info
    var $firstName;
    var $lastName;
    var $screenName;

    // Contact info
    var $email;

    // Experience
    var $experience;

    function display() {
        $output = "";
        $output .= "<h1>" . $this->firstName . " " . $this->lastName . "</h1>";
        $output .= "<ul><li>" . $this->email . "</li></ul>";

        return $output;
    }
}
?>