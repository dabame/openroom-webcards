<?php
class Webcard {
    var $id;
    var $upSide;
    var $flipSide;
    var $cardOwner;
    var $stackID;

    // write new webCard to db
    function writeNewToDb() {
        require("dbInfo.inc.php");

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die ("Connection failed: " . $conn->connect_error);
        }

        $sql = "INSERT INTO WebCards (webCardID, upSide, flipSide, cardOwner, stackID) " .
             "VALUES (NULL, '" .
             $this->upSide . "', '" .
             $this->flipSide . "', '" .
             $this->cardOwner . "', '" .
             $this->stackID . "');";

        $result = $conn->query($sql);
        if ($conn->error) {
            echo $conn->error;
        }
    }
    
    // reads specified webcard ($id) in from db ($dbInfo)
    function readFromDb($id) {
        
        // set db variables
        require("dbInfo.inc.php");

        $conn = new mysqli($servername, $username, $password, $dbname);


        if ($conn->connect_error) {
            die ("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM WebCards WHERE webCardID = '" . $id . "';";

        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $this->id = $row["webCardID"];
            $this->upSide = $row["upSide"];
            $this->flipSide = $row["flipSide"];
            $this->cardOwner = $row["cardOwner"];
            $this->stackID = $row["stackID"];
        } else {
            $this->upSide = "NOT_FOUND";
            $this->flipSide = "NOT_FOUND";
            $this->cardOwner = "NOT_FOUND";
            $this->stackID = "NOT_FOUND";
        }
    }

    // removes webcard from db
    function removeFromDb() {
        // set db variables
        require("dbInfo.inc.php");

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die ("Connection failed: " . $conn->connect_error);
        }

        $sql = "DELETE FROM WebCards WHERE webCardID = '" . $this->id . "';";

        $result = $conn->query($sql);
    }

    // return HTML of webCard ups and flips as a row of a table
    function tableDisplay() {
        $output = "";
        $output .= "<tr>";
        $output .= "<td>" . $this->upSide . "</td>";
        $output .= "<td>" . $this->flipSide . "</td>";
        $output .= "<td>";
        $output .= "<div class = 'dropdown'>";
        $output .= "<button class = 'btn btn-default dropdown-toggle' type = 'button' data-toggle = 'dropdown'>Select ";
        $output .= "<span class = 'caret'></span></button>";
        $output .= "<ul class = 'dropdown-menu'>";
        $output .= "<li><a href = 'forms/removeWebCard.php?id=" . $this->id . "&stackID=" . $this->stackID . "'>Delete</a></li>";
        $output .= "<li><em>Coming soon:</em></li>";
        $output .= "<li><a href = '#'>Edit</a></li>";
        $output .= "</ul>";
        $output .= "</div>";
        $output .= "</td>";
        $output .= "</tr>\n";
        return $output;
    }
}
?>