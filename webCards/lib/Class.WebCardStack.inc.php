<?php
require_once "Class.WebCard.inc.php";

class WebcardStack {
    var $stackID;
    var $stackName;
    var $stackOwner;
    var $description;

    var $webCards;

    // writes a stack to db as new stack
    function writeNewToDb() {
        require("dbInfo.inc.php");

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die ("Connection failed: " . $conn->connect_error);
        }

        $sql = "INSERT INTO WebCardStacks (stackID, stackName, description, stackOwnerID) " .
             "VALUES (NULL, '" .
             $this->stackName . "', '" .
             $this->description . "', '" .
             $this->stackOwner . "');";

        $result = $conn->query($sql);
        if ($conn->error) {
            echo $conn->error;
        }
    }

    // remove this stack from db
    function removeFromDb() {
        // set db variables
        require("dbInfo.inc.php");

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die ("Connection failed: " . $conn->connect_error);
        }

        $sql = "DELETE FROM WebCardStacks WHERE stackID = '" . $this->id . "';";

        $result = $conn->query($sql);
    }
    
    // reads stackID from db
    function readFromDb($id) {
        // set db variables
        require("dbInfo.inc.php");

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die ("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM WebCardStacks WHERE stackID = '" . $id . "';";

        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $this->stackID = $row["stackID"];
            $this->stackName = $row["stackName"];
            $this->stackOwner = $row["stackOwnerID"];
            $this->description = $row["description"];
        } else {
            $this->stackID = "NOT_FOUND";
            $this->stackName = "NOT_FOUND";
            $this->stackOwner = "NOT_FOUND";
            $this->description = "NOT_FOUND";
        }

        // read the Webcards for the stack into $webcards
        $sql = "SELECT webCardID FROM WebCards WHERE stackID = '" . $id . "';";

        $result = $conn->query($sql);

        $i = 0;
        
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->webCards[$i] = new WebCard();
                $this->webCards[$i++]->readFromDb($row["webCardID"]);
            }
        } 
    }
    
    // returns HTML for display of form for entering new WebCard
    function newCardForm() {
        $output = "";
        $output .= "<form id = 'newWebCard' action = 'forms/addWebCard.php'><table class = 'table'>";
        $output .= "<tr><th colspan = '2'>New Card</th></tr>";
        $output .= "<tr>";
        $output .= "<th>upSide</th><td><textarea form = 'newWebCard' name = 'upSide' class = 'form-control'></textarea></td>";
        $output .= "</tr>";
        $output .= "<tr>";
        $output .= "<th>flipSide</th><td><textarea form = 'newWebCard' name = 'flipSide' class = 'form-control'></textarea></td>";
        $output .= "</tr>";
        $output .= "<tr class = 'hidden'>";
        $output .= "<td><input readonly type = 'text' name = 'cardOwner' value = '" . $this->stackOwner . "' size = '2'/></td>";
        $output .= "<td><input readonly type = 'text' name = 'stackID' value = '" . $this->stackID . "' size = '2'/></td>";
        $output .= "</tr>";
        $output .= "<tr>";
        $output .= "<td colspan = '2'><input class = 'btn-default' type = 'submit' value = 'Create!'/></td>";
        $output .= "</tr>";
        $output .= "</table></form>";

        return $output;
    }
    
    // return HTML for display of webcards in a table
    function tableDisplay() {
        $output = "";
        $output .= "<table class = 'table'>";
        $output .= "<tr id = 'row0'>";
	$output .= "<th>" . $this->stackName . "</th>";
	$output .= "<th><button class = 'btn-primary' onclick = 'takeQuiz();'>Quiz</button></th><th>Actions</th>";
	$output .= "</tr>";
        for ($i = 0; $i < count($this->webCards); $i++) {
            $output .= $this->webCards[$i]->tableDisplay();
        }
        $output .= $this->newCardForm();
        $output .= "</table>\n";

        return $output;
    }

        

    // return HTML to display stack info as a row of table
    function statsDisplay() {
        $output = "";
        $output .= "<tr>";
        $output .= "<td><a href = '/webCards/stackView.php?id=" . $this->stackID . "'>" .$this->stackName . "</a></td>";
        $output .= "<td>" . $this->description . "<br/><br/><em>Total: " . count($this->webCards) . " cards</em></td>";
        $output .= "<td>";
        $output .= "<div class = 'dropdown'>";
        $output .= "<button class = 'btn btn-default dropdown-toggle' type = 'button' data-toggle = 'dropdown'>Select ";
        $output .= "<span class = 'caret'></span></button>";
        $output .= "<ul class = 'dropdown-menu'>";
	$output	.= "<li><a href	= '/webCards/stackView.php?id="	. $this->stackID . "'>View</a></li>";
	$output .= "<li><a href = '/webCards/forms/removeStack.php?id=" . $this->stackID . "'>Delete</a></li>";
        $output .= "<li><em>Coming soon:</em></li>";
        $output .= "<li><a href = '#'>Edit</a></li>";
        $output .= "<li><a href = '#'>Rate</a></li>";
        $output .= "<li><a href = '#'>Review</a></li>";        
        $output .= "</ul>";
        $output .= "</div>";
        $output .= "</tr>\n";

        return $output;
    }
}
?>
