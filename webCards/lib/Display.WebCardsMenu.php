<?php
// fetches all web card stacks from the db and returns them 
function fetchStacks() {
    require_once("Class.WebCardStack.inc.php");
    
    // set db variables
    require("dbInfo.inc.php");
    
    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die ("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT stackID FROM WebCardStacks;";

    $result = $conn->query($sql);

    $i = 0;
    
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $stacks[$i] = new WebCardStack();
            $stacks[$i++]->readFromDb($row["stackID"]);
        }
    }

    return $stacks;
}

// returns HTML with table contain the info for all web card stacks
function displayStacks() {
    $output = "";
    $output .= "<table class = 'table'>";
    $output .= "<tr><th>Topic</th><th>Description</th><th>Actions</th></tr>";
    
    $stacks = fetchStacks();

    for ($i = 0; $i < count($stacks); $i++) {
        $output .= $stacks[$i]->statsDisplay();
    }

    $output .= "</table>";

    return $output;
}

// returns html with form for creating a new web card stack
function newStackForm() {
    $output = "";
    $output .= "<form id = 'newStack' action = 'forms/addStack.php'><table class = 'table'>";
    $output .= "<tr><th colspan = '2'>New Stack<tr>";
    $output .= "<tr>";
    $output .= "<th>Topic</th>";
    $output .= "<td><input type = 'text' name = 'stackName' class = 'form-control' value = ''/></td>";
    $output .= "</tr>";
    $output .= "<tr>";
    $output .= "<th>Description</th>";
    $output .= "<td><textarea form = 'newStack' name = 'description' class = 'form-control'></textarea></td>";
    $output .= "<tr>";
    $output .= "</tr>";
    $output .= "<td><input class = 'btn-default' type = 'submit' value = 'Create!'/></td>";
    $output .= "<td><input type = 'text' name = 'stackOwner' class = 'hidden' size = '2' readonly value = '1'/></td>"; // change to userID
    $output .= "</tr>";
    $output .= "</table></form>";

    return $output;
}
?>