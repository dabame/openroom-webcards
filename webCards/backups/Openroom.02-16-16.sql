-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2016 at 02:24 PM
-- Server version: 10.0.21-MariaDB
-- PHP Version: 5.6.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Openroom`
--

-- --------------------------------------------------------

--
-- Table structure for table `Experience`
--

CREATE TABLE `Experience` (
  `experienceID` int(11) NOT NULL,
  `position` varchar(63) DEFAULT NULL,
  `organization` varchar(63) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `description` varchar(1023) DEFAULT NULL,
  `profileID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Profiles`
--

CREATE TABLE `Profiles` (
  `profileID` int(11) NOT NULL,
  `firstName` varchar(63) DEFAULT NULL,
  `lastName` varchar(63) DEFAULT NULL,
  `screenName` varchar(31) DEFAULT NULL,
  `email` varchar(63) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Profiles`
--

INSERT INTO `Profiles` (`profileID`, `firstName`, `lastName`, `screenName`, `email`) VALUES
(1, 'Daniel', 'Meakin', 'hotsoup', 'dbmeakin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `WebCards`
--

CREATE TABLE `WebCards` (
  `webCardID` int(11) NOT NULL,
  `upSide` varchar(2055) DEFAULT NULL,
  `flipSide` varchar(2055) DEFAULT NULL,
  `cardOwner` int(11) DEFAULT NULL,
  `stackID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `WebCards`
--

INSERT INTO `WebCards` (`webCardID`, `upSide`, `flipSide`, `cardOwner`, `stackID`) VALUES
(1, 'FTP', 'File Transfer Protocol\r\nTCP Ports 20/21', 1, 1),
(2, 'SSH', 'Secure Shell\r\nTCP Port 22', 1, 1),
(3, 'SMTP', 'Simple Mail Transfer Protocol\r\nTCP Port 25', 1, 1),
(4, 'DNS', 'Domain Name Service\r\nTCP Port 53 (zone transfers)\r\nUDP Port 53 (queries)', 1, 1),
(5, 'mesa', 'table', 1, 2),
(6, 'silla', 'seat', 1, 2),
(9, 'cuchillo', 'knife', 1, 2),
(22, 'casa', 'house\r\n', 1, 2),
(23, 'puerta', 'door', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `WebCardStacks`
--

CREATE TABLE `WebCardStacks` (
  `stackID` int(11) NOT NULL,
  `stackName` varchar(63) DEFAULT NULL,
  `description` varchar(1023) DEFAULT NULL,
  `stackOwnerID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `WebCardStacks`
--

INSERT INTO `WebCardStacks` (`stackID`, `stackName`, `description`, `stackOwnerID`) VALUES
(1, 'Security+', 'Security+ web cards!', 1),
(2, 'Spanish Nouns - Around the House', 'Some spanish nouns for common household items', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Experience`
--
ALTER TABLE `Experience`
  ADD PRIMARY KEY (`experienceID`),
  ADD KEY `profileID` (`profileID`);

--
-- Indexes for table `Profiles`
--
ALTER TABLE `Profiles`
  ADD PRIMARY KEY (`profileID`);

--
-- Indexes for table `WebCards`
--
ALTER TABLE `WebCards`
  ADD PRIMARY KEY (`webCardID`),
  ADD KEY `cardOwner` (`cardOwner`),
  ADD KEY `WebCards_ibfk_2` (`stackID`);

--
-- Indexes for table `WebCardStacks`
--
ALTER TABLE `WebCardStacks`
  ADD PRIMARY KEY (`stackID`),
  ADD KEY `stackOwnerID` (`stackOwnerID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Experience`
--
ALTER TABLE `Experience`
  MODIFY `experienceID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Profiles`
--
ALTER TABLE `Profiles`
  MODIFY `profileID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `WebCards`
--
ALTER TABLE `WebCards`
  MODIFY `webCardID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `WebCardStacks`
--
ALTER TABLE `WebCardStacks`
  MODIFY `stackID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Experience`
--
ALTER TABLE `Experience`
  ADD CONSTRAINT `Experience_ibfk_1` FOREIGN KEY (`profileID`) REFERENCES `Profiles` (`profileID`);

--
-- Constraints for table `WebCards`
--
ALTER TABLE `WebCards`
  ADD CONSTRAINT `WebCards_ibfk_1` FOREIGN KEY (`cardOwner`) REFERENCES `Profiles` (`profileID`),
  ADD CONSTRAINT `WebCards_ibfk_2` FOREIGN KEY (`stackID`) REFERENCES `WebCardStacks` (`stackID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `WebCardStacks`
--
ALTER TABLE `WebCardStacks`
  ADD CONSTRAINT `WebCardStacks_ibfk_1` FOREIGN KEY (`stackOwnerID`) REFERENCES `Profiles` (`profileID`),
  ADD CONSTRAINT `WebCardStacks_ibfk_2` FOREIGN KEY (`stackOwnerID`) REFERENCES `Profiles` (`profileID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
